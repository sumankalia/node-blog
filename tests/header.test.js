const puppeteer = require("puppeteer");

test("Adds two numbers", () => {
  const sum = 1 + 2;

  expect(sum).toEqual(3);
});

let browser, page;
beforeEach(async () => {
  browser = await puppeteer.launch({
    headless: false
  });

  afterEach(async () => {
    await browser.close();
  });

  page = await browser.newPage();
  await page.goto("localhost:3000");
});

test("We can launch a browser", async () => {
  const text = await page.$eval("a.brand-logo", el => el.innerHTML);

  expect(text).toEqual("Blogster");
});
